package ie.tcd.scss;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;

public class LCAJavaTest {

    @Test
    public void testNode() {
        Node test = new Node(0);
        assertEquals(test.key, 0);
        assertTrue(test.children.isEmpty());
    }

    @Test
    public void testAddingNodes(){
        Node test = new Node(0);
        test.addChild(new Node(1));
        assertEquals(test.children.get(0).key,1);
        test.addChild(new Node(2));
        assertEquals(test.children.get(1).key,2);
    }

    @Test
    public void testFindPath() {
        LCAJava test = new LCAJava(1);

        //test that if it finds the path then it returns true
        assertTrue(test.findPath(test.root, 1, new ArrayList<Integer>()));

        test.root = mkTestTree(test.root);
        ArrayList<Integer> path = new ArrayList<Integer>();
        //Test that it finds a path if the given key exists in the tree
        assertTrue(test.findPath(test.root, 4, path));
        path = new ArrayList<Integer>();
        assertTrue(test.findPath(test.root, 3, path));

        path = new ArrayList<Integer>();
        //Test that it fails when passed key that isn't in tree
        assertFalse(test.findPath(test.root, 10, path));

        //Testing done :)
    }

    @Test
    public void testFindLCA() {
        LCAJava test = new LCAJava(0);
        Node root = null;
        //test if bad value passed that it returns -1
        assertEquals(test.findLCA(1,2),-1);

        test.root = new Node(1);
        //test if searching for same key works
        assertEquals(test.findLCA(1,1),1);

        test.root = mkTestTree(root);
        //Establish tree and test full functionality
        assertEquals(test.findLCA(3,4),1);
        assertEquals(test.findLCA(5,2),2);
        assertEquals(test.findLCA(2,5),2);
        assertEquals(test.findLCA(4,5),4);
    }


    @Test
    public void testMakingTree(){
        Node root = new Node(1);
        Node b = new Node(2);
        Node c = new Node(3);
        Node d = new Node(4);
        Node e = new Node(5);

        root.addChild(b);
        root.addChild(c);
        root.addChild(d);
        root.addChild(e);

        b.addChild(d);

        c.addChild(d);
        c.addChild(e);

        d.addChild(e);

        assertEquals(root.children.get(root.children.size()-1).key,root.children.get(0).children.get(0).children.get(0).key);
    }
    public Node mkTestTree(Node root) {
        root = new Node(1);
        Node b = new Node(2);
        Node c = new Node(3);
        Node d = new Node(4);
        Node e = new Node(5);

        root.addChild(b);
        root.addChild(c);
        root.addChild(d);
        root.addChild(e);

        b.addChild(d);

        c.addChild(d);
        c.addChild(e);

        d.addChild(e);
        return root;
    }
}