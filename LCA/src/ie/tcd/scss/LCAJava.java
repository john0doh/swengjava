package ie.tcd.scss;

import java.util.ArrayList;

// A Binary Tree node 
class Node {
    int key;
    ArrayList<Node>children;

    Node(int key) {
        this.key = key;
        children = new ArrayList<Node>();
    }

    public void addChild(Node n){
        children.add(n);
    }

    public boolean directsTo(int n){
        for(int i = 0; i<children.size();i++){
            if(children.get(i).key==i)
                return true;
            if(children.get(i).directsTo(n))
                return true;
        }
        return false;
    }
}

public class LCAJava {

    LCAJava(int key){
        this.root = new Node(key);
    }

    Node root;

    ArrayList<Integer> path1, path2;

    public int findLCA(int key1, int key2) {

        path1 = new ArrayList<Integer>();
        path2 = new ArrayList<Integer>();

        if (!findPath(root, key1, path1) || !findPath(root, key2, path2))
            return -1;

        int i = 0;
        while (i < path1.size() && i < path2.size()) {
            if (path1.get(i) != path2.get(i))
                break;
            i++;
        }
        return path1.get(i - 1);

    }

    public boolean findPath(Node root, int key, ArrayList<Integer> path) {

        if (root == null)
            return false;

        path.add(root.key);

        if (root.key == key)
            return true;

        for(int i =0;i<root.children.size();i++){
            if(findPath(root.children.get(i),key,path))
                return true;
        }

        path.remove(path.size() - 1);

        return false;
    }

    public static void main(String[] args) {

    }
}